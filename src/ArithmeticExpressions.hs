module ArithmeticExpressions where

import Data.Map     as Map
import Control.Monad.Reader

data Expr = Lit Int
          | Var String
          | Add Expr Expr
          | Mul Expr Expr
          | Assign String Int Expr

evaluate :: Expr -> Reader (Map String Int) (Maybe Int)
evaluate (Lit a) = return $ Just a
evaluate (Var name) = reader $ \v ->  Map.lookup name v
evaluate (Add e1 e2) = evaluate e1 >>= \val1 -> evaluate e2 >>= \val2 -> return $ liftM2 (+) val1 val2
evaluate (Mul e1 e2) = evaluate e1 >>= \val1 -> evaluate e2 >>= \val2 -> return $ liftM2 (*) val1 val2
evaluate (Assign name value expr) = reader $ \v -> runReader (evaluate expr) (Map.insert name value v)

initMap :: Map String Int
initMap = Map.singleton "x" 1