{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ViewPatterns  #-}

module TreeWithWriter where

import           Control.Monad.Writer (Writer, execWriter, writer)
import           Data.Char            (isSpace)
import           Data.List            (maximum)

data Tree a = Leaf | Node a (Tree a) (Tree a)
    deriving (Functor, Show)

leaf = "Leaf\n"
equ = "EQ\n"
ltn = "LT\n"
gtn = "GT\n"

println :: Writer String a -> IO ()
println b = putStr $ execWriter  b

find :: (Ord a, Show a) => Tree a -> a -> Writer String Bool
find Leaf _ = writer (False, leaf)
find (Node value left right) x = case compare x value of
    EQ -> writer (True, equ)
    LT -> find left x >>= \b -> writer (b, ltn ++ verticalPrint left)
    GT -> find right x >>= \b -> writer (b, gtn ++ verticalPrint right)

insert :: (Ord a, Show a) => Tree a -> a -> Writer String (Tree a)
insert Leaf x = writer (Node x Leaf Leaf, leaf)
insert (Node value left right) x = case compare x value of
    LT        -> insert left x >>= \b -> writer (Node value b right, ltn ++ verticalPrint left)
    _ -> insert right x >>= \b -> writer (Node value left b, gtn ++ verticalPrint right)

delete :: (Ord a, Show a) => Tree a -> a -> Writer String (Tree a)
delete Leaf x = writer (Leaf, leaf)
delete (Node value left right) x = case compare x value of
    LT -> delete left x >>= \b -> writer (Node value b right, ltn ++ verticalPrint left)
    GT -> delete right x >>= \b -> writer (Node value left b, gtn ++ verticalPrint right)
    EQ -> case (left, right) of
        (Leaf, _)  -> writer (right, equ)
        (_, Leaf)  -> writer (left, equ)
        _          -> let min = findMin right in min >>= \c -> (delete right c >>= \b -> ((\ d -> writer (d, equ)) (Node c left b)))

findMin :: (Ord a, Show a) => Tree a -> Writer String a
findMin Leaf = undefined
findMin (Node value Leaf _) = writer (value, "min = " ++ show value ++ "\n")
findMin (Node _ left _) = findMin left >>= \b -> writer (b, ltn ++ verticalPrint left)

toList :: (Ord a, Show a) => Tree a -> Writer String [a]
toList Leaf = writer ([], "")
toList (Node value left right) = toList left >>= \a -> toList right >>= \b -> writer(a ++ value:b, "concat " ++ show a ++ " with " ++ show b)

fromList :: (Ord a, Show a) => [a] -> Writer String (Tree a)
fromList = foldr (\x y -> y >>= \c -> insert c x) (writer (Leaf, ""))
















verticalPrint :: Show a => Tree a -> String
verticalPrint = unlines . rowPrinter . fmap show

type TreeRows = [String]

rowPrinter :: Tree String -> TreeRows
rowPrinter Leaf                  = []
rowPrinter (Node key Leaf  Leaf) = [key]
rowPrinter (Node key left  Leaf) = connectOneChild key left
rowPrinter (Node key Leaf right) = connectOneChild key right
rowPrinter (Node key left right) =
    let lr@(ltop:_)  = rowPrinter left
        rr@(rtop:_)  = rowPrinter right

        ledgePos     = labelMidPosition ltop
        redgePos     = labelMidPosition rtop

        leftWidth    = 1 + maximum (map length lr)
        connectorLen = leftWidth + redgePos - 1 - ledgePos
        connector    = nspaces (ledgePos + 1) ++ replicate connectorLen '-'

        leftSubTree  = upEdge ledgePos : lr
        rightSubTree = upEdge redgePos : rr
        childrenRows = mergeChildren leftWidth leftSubTree rightSubTree
    in attachRows key (connector:childrenRows)

connectOneChild :: String -> Tree String -> TreeRows
connectOneChild label (rowPrinter -> rows) = attachRows label rows

attachRows :: String -> TreeRows -> TreeRows
attachRows label subTree@(top:_) =
    let labelMid    = labelMidPosition label
        topLabelMid = labelMidPosition top
        shortEdge   = upEdge topLabelMid
        subTreeRows = shortEdge : subTree
        padding     = abs (topLabelMid - labelMid)
        (cur, tree) = if topLabelMid < labelMid
                      then (label, map (moveRight padding) subTreeRows)
                      else (moveRight padding label, subTreeRows)
    in cur : tree
attachRows _ _ = error "Algorithm error: attach call on empty subtree"

-----------------------------------------
{- Helpers and other utility functions -}
-----------------------------------------

middle :: Int -> Int
middle x = x `div` 2

labelMidPosition :: String -> Int
labelMidPosition label =
    let (spaces, value) = span isSpace label
        valueMid        = middle $ length value
    in length spaces + valueMid

nspaces :: Int -> String
nspaces n = replicate n ' '

upEdge :: Int -> String
upEdge padding = nspaces padding ++ "|"

moveRight :: Int -> String -> String
moveRight n = (nspaces n ++)

fillRight :: Int -> String -> String
fillRight len s = s ++ nspaces (len - length s)

mergeChildren :: Int -> TreeRows -> TreeRows -> TreeRows
mergeChildren lWidth = scanDown
  where
    scanDown :: TreeRows -> TreeRows -> TreeRows
    scanDown    []     []  = []
    scanDown     l     []  = l
    scanDown    []  (r:rs) = (nspaces   lWidth   ++ r) : scanDown [] rs
    scanDown (l:ls) (r:rs) = (fillRight lWidth l ++ r) : scanDown ls rs

