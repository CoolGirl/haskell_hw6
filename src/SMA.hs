module SMA where

import           Control.Monad.State

moving :: Int -> [Double] -> [Double]
moving winWidth l = zipWith (/) (partialSums winWidth l) (divisors winWidth)

partialSums :: Int -> [Double] -> [Double]
partialSums winWidth l = reverse ((\(_, _, result) -> result) (execState runState (foldr1 (>>) (map changeState l))
               (0, replicate winWidth 0.0, [])))

divisors :: Int -> [Double]
divisors winWidth = map (\x -> fromIntegral (min x winWidth)) [1..]

changeState :: Double -> State (Double, [Double], [Double]) ()
changeState newVal = state $ \(sum, x:xs, result) -> let nSum = sum - x + newVal in
 ((), (nSum, xs ++ [newVal], nSum:result))
